-- MariaDB dump 10.17  Distrib 10.5.4-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: pol
-- ------------------------------------------------------
-- Server version	10.5.4-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accesos`
--

DROP TABLE IF EXISTS `accesos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accesos` (
  `acceso_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT 0,
  `rol_id` int(11) DEFAULT 0,
  `acceso_fecha` int(11) DEFAULT 0,
  `acceso_usuario` int(11) DEFAULT 0,
  PRIMARY KEY (`acceso_id`),
  KEY `FK_accesos_rol_id` (`rol_id`),
  KEY `FK_accesos_menu_id` (`menu_id`),
  CONSTRAINT `FK_accesos_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`menu_id`),
  CONSTRAINT `FK_accesos_rol_id` FOREIGN KEY (`rol_id`) REFERENCES `roles` (`rol_id`),
  CONSTRAINT `fk_acceso_menu` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`menu_id`),
  CONSTRAINT `fk_acceso_rol` FOREIGN KEY (`rol_id`) REFERENCES `roles` (`rol_id`)
) ENGINE=InnoDB AUTO_INCREMENT=420 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accesos`
--

LOCK TABLES `accesos` WRITE;
/*!40000 ALTER TABLE `accesos` DISABLE KEYS */;
INSERT INTO `accesos` VALUES (410,1,1,0,0),(411,2,1,0,0),(412,3,1,0,0),(413,98,1,0,0),(414,99,1,0,0),(415,100,1,0,0),(416,101,1,0,0),(417,102,1,0,0),(418,103,1,0,0),(419,104,1,NULL,NULL);
/*!40000 ALTER TABLE `accesos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cotizacion`
--

DROP TABLE IF EXISTS `cotizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cotizacion` (
  `idcotizacion` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `moneda` varchar(1) DEFAULT 'E',
  `idusuario` int(11) DEFAULT NULL,
  `cotizacionventa` int(11) DEFAULT NULL,
  `cotizacioncompra` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcotizacion`),
  UNIQUE KEY `cotizacion_un` (`fecha`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cotizacion`
--

LOCK TABLES `cotizacion` WRITE;
/*!40000 ALTER TABLE `cotizacion` DISABLE KEYS */;
INSERT INTO `cotizacion` VALUES (5,'2023-01-17',NULL,NULL,7420,NULL);
/*!40000 ALTER TABLE `cotizacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curso` (
  `idcurso` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `detalle1` varchar(200) NOT NULL,
  `detalle2` varchar(200) DEFAULT NULL,
  `detalle3` varchar(300) DEFAULT NULL,
  `preciopy` double NOT NULL,
  `preciouss` double NOT NULL,
  `estado` varchar(50) NOT NULL,
  `idusuario` int(11) NOT NULL DEFAULT 0,
  `idespecialidad` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcurso`) USING BTREE,
  KEY `FK_curso_usuario` (`idusuario`),
  KEY `FK_curso_especialidad` (`idespecialidad`),
  CONSTRAINT `FK_curso_especialidad` FOREIGN KEY (`idespecialidad`) REFERENCES `especialidad` (`idespecialidad`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso`
--

LOCK TABLES `curso` WRITE;
/*!40000 ALTER TABLE `curso` DISABLE KEYS */;
INSERT INTO `curso` VALUES (2,'Analisis Estadisticos con Excel','En este curso en línea el estudiante aprenderá los conceptos estadísticos básicos para realizar un análisis aplicado de datos, haciendo los cálculos en Excel','','',700000,10000,'Habilitado',0,1),(3,'Visualización de Datos y Storytelling','Aprende en este curso en línea que es la visualización de datos, sus usos; los elementos que la conforman y la forma de poder utilizarla para el apoyo en la toma de las mejores decisiones para las','empresas.','',900000,15000,'Habilitado',0,1),(4,'Introducción a la ciencia de datos y sus app.','Por favor ten en cuenta: Los estudiantes que completen con éxito este curso de IBM ahora pueden obtener una insignia digital de habilidades: una credencial detallada, verificable y digital','','',2000000,30000,'Habilitado',0,1),(5,'Desarrollo y gestión de proyectos informático','Gestiona de manera eficiente y participa con éxito en el desarrollo de un proyecto informático','','',1500000,25000,'Habilitado',0,2),(6,'pintura sobre tela','1231','123','123',100000,2000,'Habilitado',0,1);
/*!40000 ALTER TABLE `curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `especialidad`
--

DROP TABLE IF EXISTS `especialidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `especialidad` (
  `idespecialidad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) DEFAULT NULL,
  `fechaalta` varchar(100) DEFAULT NULL,
  `usuarioalta` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idespecialidad`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `especialidad`
--

LOCK TABLES `especialidad` WRITE;
/*!40000 ALTER TABLE `especialidad` DISABLE KEYS */;
INSERT INTO `especialidad` VALUES (1,'Base de Datos',NULL,NULL),(2,'Programacion',NULL,NULL),(3,'Redes',NULL,NULL);
/*!40000 ALTER TABLE `especialidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inscripcion`
--

DROP TABLE IF EXISTS `inscripcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inscripcion` (
  `idiscripcion` int(11) NOT NULL AUTO_INCREMENT,
  `fechainscripcion` date NOT NULL,
  `idpersona` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL,
  PRIMARY KEY (`idiscripcion`),
  KEY `FK_inscripcion_curso` (`idcurso`),
  KEY `FK_inscripcion_persona` (`idpersona`),
  KEY `idiscripcion` (`idiscripcion`),
  CONSTRAINT `FK_inscripcion_curso` FOREIGN KEY (`idcurso`) REFERENCES `curso` (`idcurso`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inscripcion`
--

LOCK TABLES `inscripcion` WRITE;
/*!40000 ALTER TABLE `inscripcion` DISABLE KEYS */;
/*!40000 ALTER TABLE `inscripcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_nombre` varchar(20) NOT NULL DEFAULT '0',
  `menu_orden` int(11) NOT NULL DEFAULT 0,
  `menu_tipo` varchar(20) NOT NULL DEFAULT '0',
  `menu_componente` varchar(20) NOT NULL DEFAULT '0',
  `menu_label` varchar(20) NOT NULL DEFAULT '0',
  `menu_icon` varchar(20) NOT NULL DEFAULT '0',
  `menu_url` varchar(45) NOT NULL DEFAULT '0',
  `menu_padre` int(11) NOT NULL DEFAULT 0,
  `menu_usuario` varchar(20) NOT NULL DEFAULT '0',
  `menu_fecha_alta` date NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'Archivo',1000,'M','submenu','Archivo','0','\\N',0,'juan','2016-04-10'),(2,'Salir',1001,'M','menuitem','Salir','0','/Pol/faces/salir.xhtml',1,'juan','2016-04-10'),(3,'Configuracion',2000,'M','submenu','Configuracion','0','\\N',0,'GUSTAVO','2016-04-10'),(98,'Sistema',3000,'M','submenu','Sistema','0','\\N',0,'juan','2023-01-12'),(99,'Roles',3001,'M','menuitem','Roles','0','/Pol/faces/roles.xhtml',98,'juan','2023-01-12'),(100,'Menu',3002,'M','menuitem','Menus','0','/Pol/faces/menus.xhtml',98,'juan','2023-01-12'),(101,'Accesos',3003,'M','menuitem','Accesos','0','/Pol/faces/accesos.xhtml',98,'juan','2023-01-12'),(102,'Especialidades',2001,'M','menuitem','Especialidades','0','/Pol/faces/especialidades.xhtml',3,'juan','2023-01-12'),(103,'Cursos',2002,'M','menuitem','Cursos','0','/Pol/faces/cursos.xhtml',3,'juan','2023-01-12'),(104,'Cotizaciones',2003,'M','menuitem','Cotizaciones','0','/Pol/faces/cotizaciones.xhtml',3,'juan','2023-01-17');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagos`
--

DROP TABLE IF EXISTS `pagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagos` (
  `idpago` int(11) NOT NULL AUTO_INCREMENT,
  `idinscripcion` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `importe` double DEFAULT NULL,
  `moneda` varchar(3) DEFAULT NULL,
  `nroreferencia` varchar(12) DEFAULT NULL,
  `codautorizacion` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`idpago`),
  KEY `pagos_FK` (`idinscripcion`),
  CONSTRAINT `pagos_FK` FOREIGN KEY (`idinscripcion`) REFERENCES `inscripcion` (`idiscripcion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagos`
--

LOCK TABLES `pagos` WRITE;
/*!40000 ALTER TABLE `pagos` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `idpersona` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `apellido` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `fechanac` date NOT NULL,
  `email` varchar(30) CHARACTER SET utf8mb4 NOT NULL,
  `tipodocumento` varchar(10) CHARACTER SET utf8mb4 NOT NULL,
  `nrodocumento` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `pais` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `telefono` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8mb4_danish_ci DEFAULT NULL,
  PRIMARY KEY (`idpersona`),
  UNIQUE KEY `persona_un` (`email`),
  KEY `idpersona` (`idpersona`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (1,'Juan','Rojas','1984-04-26','juangelrc@gmail.com','C','3951817','PYG','0975174531','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3','A');
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `rol_id` int(11) NOT NULL AUTO_INCREMENT,
  `rol_nombre` varchar(20) DEFAULT '0',
  `rol_usuario_alta` varchar(20) DEFAULT '0',
  `rol_fecha_alta` date DEFAULT NULL,
  PRIMARY KEY (`rol_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin','0',NULL),(2,'Alumno','0',NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `idpersona` int(11) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(30) NOT NULL,
  PRIMARY KEY (`email`),
  KEY `idusuario` (`idusuario`),
  KEY `FK_usuario_persona` (`idpersona`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_roles`
--

DROP TABLE IF EXISTS `usuarios_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios_roles` (
  `usuario_rol_id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_codigo` int(11) DEFAULT 0,
  `rol_id` int(11) DEFAULT 0,
  PRIMARY KEY (`usuario_rol_id`),
  KEY `fk_usuarios` (`usuario_codigo`),
  KEY `fk_roles` (`rol_id`),
  CONSTRAINT `FK_usuarios_roles_personas` FOREIGN KEY (`usuario_codigo`) REFERENCES `persona` (`idpersona`),
  CONSTRAINT `fk_roles` FOREIGN KEY (`rol_id`) REFERENCES `roles` (`rol_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13660 DEFAULT CHARSET=utf8 COMMENT='Roles de Usuario del Sistema';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_roles`
--

LOCK TABLES `usuarios_roles` WRITE;
/*!40000 ALTER TABLE `usuarios_roles` DISABLE KEYS */;
INSERT INTO `usuarios_roles` VALUES (1,1,1);
/*!40000 ALTER TABLE `usuarios_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'pol'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-03-10 21:12:18
